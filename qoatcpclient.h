#ifndef QOATCPCLIENT_H
#define QOATCPCLIENT_H

#include <QObject>
#include <QtCore>
#include <QTcpSocket>
#include "qoatcpclient.h"
//#include "qsnshapes.h"

class QOATCPclient : public QObject
{
    Q_OBJECT

public:
    explicit QOATCPclient(QObject *parent = 0);
    ~QOATCPclient();
    bool isEnabled();
    void setEnabled(bool enable);
    quint16 getPort();
    void setPort(quint16 port);
    QString getAddress();
    void setAddress(QString address);
    bool isConnected();
    void setLoginAndPassword(QString clientlogin, QString pass);
    QString getLogin();
    QString getPassword();
    void setDeviceName(QString name);

signals:
    void changeState(QString state);

public slots:
    //----BM

    void saveSettings(QSettings *settings);
    void loadSettings(QSettings *settings);
    void saveSettingsStream(QDataStream *stream);
    void loadSettingsStream(QDataStream *stream);
    void checkMac(QString mac);
    void disconnectFromServer();

private slots:
    void readSize();
    void writeSize(quint32 size);
    void readyRead();
    void disconnected();
    void sendBlock(QByteArray *block);
    void readedBlock(QByteArray *block);

private:
    quint16 deviceAddress;
	bool clientEnable;
    QString clientaddress;
    quint16 clientport;
    QTcpSocket *clientSocket;
    quint8 caseMode;
    quint32 dataBlockSize;
    bool authorized;
    QString clientlogin;
    QString clientpassword;
	QTime timeDelay;
    QString deviceName;

    void macRequest(QString mac);
    void readAnswerPath();
    void state_1();
    void state_2();

};

#endif // QOATCPCLIENT_H

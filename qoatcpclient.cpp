#include "qoatcpclient.h"


QOATCPclient::QOATCPclient(QObject *parent) :
    QObject(parent)
{
    setObjectName(QString(tr("TCPclient")));
    deviceName = QCoreApplication::applicationName();
    clientSocket = new QTcpSocket();
    clientaddress = QLatin1String("localhost");
    clientport = 9999;
    clientEnable = false;
    caseMode = 1;
    authorized = false;
    QObject::connect(clientSocket, SIGNAL(readyRead()),this, SLOT(readyRead()));

}

QOATCPclient::~QOATCPclient()
{
    QObject::disconnect(clientSocket, SIGNAL(readyRead()),this, SLOT(readyRead()));
    delete clientSocket;
}

void QOATCPclient::state_1()
{
    if (clientSocket->bytesAvailable() < 1) return;
    char p;
    clientSocket->read(&p, 1);
    if (p == char(115)) caseMode ++;
    readyRead();
}

void QOATCPclient::state_2()
{
    if (clientSocket->bytesAvailable() < 1) return;
    char p;
    clientSocket->read(&p, 1);
    if (p == char(110)) caseMode ++;
    else caseMode = 1;
    readyRead();
}

void QOATCPclient::saveSettings(QSettings *settings)
{
    settings->setValue(QLatin1String("TCP_Enabled"), isEnabled());
    settings->setValue(QLatin1String("TCP_Port"), getPort());
    settings->setValue(QLatin1String("TCP_Address"), getAddress());
}

void QOATCPclient::loadSettings(QSettings *settings)
{
    setEnabled(settings->value(QLatin1String("TCP_Enabled"), isEnabled()).toBool());
    setPort(static_cast<quint16>(settings->value(QLatin1String("TCP_Port"), getPort()).toUInt()));
    setAddress(settings->value(QLatin1String("TCP_Address"), getAddress()).toString());

}

void QOATCPclient::saveSettingsStream(QDataStream *stream)
{
    *stream << isEnabled();
    *stream << getPort();
    *stream << getAddress();
}

void QOATCPclient::loadSettingsStream(QDataStream *stream)
{
    bool enable;
    QString s;
    quint16 i;
    *stream >> enable;
    setEnabled(enable);
    *stream >> i;
    setPort(i);
    *stream >> s;
    setAddress(s);
}

void QOATCPclient::checkMac(QString mac)
{
    if (!clientEnable) return;
    bool connected;
    clientSocket->connectToHost(clientaddress, clientport);
    connected = clientSocket->waitForConnected(3000);
    if (!connected) {
        //info(0, QString(tr("Connect to server failed")));
        changeState("Error connect");
        return;
    }
    caseMode = 1;
    timeDelay.start();
    QObject::connect(clientSocket, SIGNAL(disconnected()),this, SLOT(disconnected()));
    readyRead();
    macRequest(mac);
}

bool QOATCPclient::isEnabled()
{
    return clientEnable;
}

void QOATCPclient::setEnabled(bool enable)
{
    clientEnable = enable;
}

quint16 QOATCPclient::getPort()
{
    return clientport;
}

void QOATCPclient::setPort(quint16 port)
{
    clientport = port;
}

bool QOATCPclient::isConnected()
{
    return clientSocket->isOpen();
}

void QOATCPclient::setLoginAndPassword(QString login, QString pass)
{
    clientlogin = login;
    clientpassword = pass;
}

QString QOATCPclient::getLogin()
{
    return clientlogin;
}

QString QOATCPclient::getPassword()
{
    return clientpassword;
}

void QOATCPclient::setDeviceName(QString name)
{
    deviceName = name;
}

QString QOATCPclient::getAddress()
{
    return clientaddress;
}

void QOATCPclient::setAddress(QString address)
{
    clientaddress = address;
}

void QOATCPclient::readSize()
{
    if (clientSocket->bytesAvailable() < 4) return;
    QByteArray ba;
    ba = clientSocket->read(4);
    QDataStream stream(&ba, QIODevice::ReadOnly);
    stream.setVersion(QDataStream::Qt_4_7);
    stream >> dataBlockSize;
    caseMode ++;
    readyRead();
}

void QOATCPclient::writeSize(quint32 size)
{
    QByteArray block;
    QDataStream stream(&block, QIODevice::ReadWrite);
    stream << quint8(115);
    stream << quint8(110);
    stream << size;
    clientSocket->write(block);
    clientSocket->flush();
}

void QOATCPclient::readyRead()
{
    switch (caseMode)
    {
    case 1:state_1(); break;
    case 2:state_2(); break;
    case 3:readSize(); break;
    case 4:readAnswerPath(); break;
    }
}

void QOATCPclient::disconnected()
{
    clientSocket->close();
}

void QOATCPclient::sendBlock(QByteArray *block)
{
    writeSize(static_cast<quint32>(block->size()));
    clientSocket->write(*block);
    clientSocket->flush();
    caseMode = 1;
    readyRead();
}

void QOATCPclient::disconnectFromServer()
{
    QObject::disconnect(clientSocket, SIGNAL(disconnected()),this, SLOT(disconnected()));
    clientSocket->disconnectFromHost();
    clientSocket->close();
}

void QOATCPclient::readAnswerPath()
{
    if (clientSocket->bytesAvailable() < static_cast<qint16>(dataBlockSize)) return;
    QByteArray block;
    block = clientSocket->read(static_cast<qint16>(dataBlockSize));
    readedBlock(&block);
    caseMode = 1;
    readyRead();
}

void QOATCPclient::readedBlock(QByteArray *block)
{
    QDataStream stream(block, QIODevice::ReadWrite);
    stream.setVersion(QDataStream::Qt_4_7);
    QString state = "unknown";
    stream >> state;
    changeState(state);
    disconnectFromServer();
}

void QOATCPclient::macRequest(QString mac)
{
    QByteArray block;
    QDataStream stream(&block, QIODevice::ReadWrite);
    stream.setVersion(QDataStream::Qt_4_7);
    stream << mac;
    sendBlock(&block);
}

#include "mainwindow.h"
#include <QApplication>
#include <QTranslator>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    QTranslator appTranslator;
    QCoreApplication::setOrganizationName(QLatin1String("SignalNet"));
    QCoreApplication::setApplicationName(QLatin1String("OAClient"));
    QCoreApplication::setApplicationVersion("0.1.0.1");
    appTranslator.load(QLatin1String("oaclient_")+QLocale::system().name(),"://");
    a.installTranslator(&appTranslator);
    MainWindow w;
    return a.exec();
}

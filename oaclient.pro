#-------------------------------------------------
#
# Project created by QtCreator 2017-10-02T09:00:00
#
#-------------------------------------------------

QT       += core gui network widgets

unix:!macx:LIBS += -lXtst -lX11

TARGET = oaclient
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    qsnshapes.cpp \
    qsnmediakey.cpp \
    qoatcpclient.cpp

HEADERS  += mainwindow.h \
    qsnshapes.h \
    qsnmediakey.h \
    qoatcpclient.h

FORMS    += mainwindow.ui

RESOURCES += \
    resources.qrc

win32 {
        RC_FILE += applico.rc
        OTHER_FILES += applico.rc
}

TRANSLATIONS = oaclient_ru_RU.ts

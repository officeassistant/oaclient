<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="ru_RU">
<context>
    <name>MainWindow</name>
    <message>
        <source>SN Agent settings</source>
        <translation type="vanished">Настройки SN Агента</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="17"/>
        <source>SN Agent</source>
        <translation>SN Агент</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="61"/>
        <source>Connect to TCP server</source>
        <translation>Подключение к серверу TCP</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="93"/>
        <location filename="mainwindow.ui" line="209"/>
        <source>Address</source>
        <translation>Адрес</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="100"/>
        <location filename="mainwindow.ui" line="216"/>
        <source>localhost</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="111"/>
        <location filename="mainwindow.ui" line="227"/>
        <source>Port</source>
        <translation>Порт</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="141"/>
        <source>Login</source>
        <translation>Имя пользователя</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="155"/>
        <source>Password</source>
        <translation>Пароль</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="177"/>
        <source>Connect to UDP server</source>
        <translation>Подключение к UDP-серверу</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="257"/>
        <source>key</source>
        <translation>ключь</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="307"/>
        <source>Apply</source>
        <translation>Применить</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="314"/>
        <location filename="mainwindow.ui" line="434"/>
        <source>Cancel</source>
        <translation>Отмена</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="427"/>
        <source>Set</source>
        <translation>Установить</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="448"/>
        <source>Connect</source>
        <translation>Подключить</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="453"/>
        <location filename="mainwindow.ui" line="456"/>
        <source>Disconnect</source>
        <translation>Отключить</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="461"/>
        <location filename="mainwindow.ui" line="464"/>
        <source>Exit</source>
        <translation>Выход</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="469"/>
        <location filename="mainwindow.ui" line="472"/>
        <source>Settings</source>
        <translation>Настройки</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="481"/>
        <location filename="mainwindow.ui" line="484"/>
        <location filename="mainwindow.cpp" line="363"/>
        <source>Temperature</source>
        <translation>Температура</translation>
    </message>
    <message>
        <location filename="mainwindow.ui" line="493"/>
        <location filename="mainwindow.ui" line="496"/>
        <location filename="mainwindow.cpp" line="378"/>
        <source>Thermostat</source>
        <translation>Термостат</translation>
    </message>
    <message>
        <location filename="mainwindow.cpp" line="353"/>
        <location filename="mainwindow.cpp" line="356"/>
        <source>Bell</source>
        <translation>Звонок</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="qsnshapes.cpp" line="123"/>
        <source>Hex code 32 bit</source>
        <translation></translation>
    </message>
    <message>
        <location filename="qsnshapes.cpp" line="127"/>
        <source>Variant (Any)</source>
        <translation></translation>
    </message>
    <message>
        <location filename="qsnshapes.cpp" line="134"/>
        <source>°C</source>
        <translation>°C</translation>
    </message>
    <message>
        <location filename="qsnshapes.cpp" line="139"/>
        <source>Wt*h</source>
        <translation>Вт*ч</translation>
    </message>
    <message>
        <location filename="qsnshapes.cpp" line="140"/>
        <location filename="qsnshapes.cpp" line="142"/>
        <source>%</source>
        <translation></translation>
    </message>
    <message>
        <location filename="qsnshapes.cpp" line="141"/>
        <source>hPa</source>
        <translation>гПа</translation>
    </message>
    <message>
        <location filename="qsnshapes.cpp" line="143"/>
        <source>mm</source>
        <translation>мм</translation>
    </message>
    <message>
        <location filename="qsnshapes.cpp" line="144"/>
        <source>lx</source>
        <translation>люкс</translation>
    </message>
    <message>
        <location filename="qsnshapes.cpp" line="145"/>
        <source>l</source>
        <translation>л</translation>
    </message>
    <message>
        <location filename="qsnshapes.cpp" line="146"/>
        <source>g</source>
        <translation>г</translation>
    </message>
    <message>
        <location filename="qsnshapes.cpp" line="165"/>
        <source>Energy</source>
        <translation>Енергия</translation>
    </message>
    <message>
        <location filename="qsnshapes.cpp" line="176"/>
        <source>Code</source>
        <translation>Код</translation>
    </message>
    <message>
        <location filename="qsnshapes.cpp" line="178"/>
        <source>snir</source>
        <translation>SNIR</translation>
    </message>
    <message>
        <location filename="qsnshapes.cpp" line="179"/>
        <source>uid</source>
        <translation></translation>
    </message>
    <message>
        <location filename="qsnshapes.cpp" line="255"/>
        <source>TRUE</source>
        <translation>ИСТИНА</translation>
    </message>
    <message>
        <location filename="qsnshapes.cpp" line="256"/>
        <source>FALSE</source>
        <translation>ЛОЖЬ</translation>
    </message>
    <message>
        <location filename="qsnshapes.cpp" line="259"/>
        <source>ON</source>
        <translation>ВКЛ</translation>
    </message>
    <message>
        <location filename="qsnshapes.cpp" line="260"/>
        <source>OFF</source>
        <translation>ВЫКЛ</translation>
    </message>
    <message>
        <location filename="qsnshapes.cpp" line="263"/>
        <source>YES</source>
        <translation>ДА</translation>
    </message>
    <message>
        <location filename="qsnshapes.cpp" line="264"/>
        <source>NO</source>
        <translation>НЕТ</translation>
    </message>
    <message>
        <location filename="qsnshapes.cpp" line="267"/>
        <source>OPEN</source>
        <translation>ОТКРЫТО</translation>
    </message>
    <message>
        <location filename="qsnshapes.cpp" line="268"/>
        <source>CLOSE</source>
        <translation>ЗАКРЫТО</translation>
    </message>
    <message>
        <location filename="qsnshapes.cpp" line="285"/>
        <source>%1, device address: %2</source>
        <translation>%1, адрес устройства: %2</translation>
    </message>
    <message>
        <location filename="qsnshapes.cpp" line="399"/>
        <source>Canceling alert</source>
        <oldsource>Canceling alarm</oldsource>
        <translation>Отмена оповещения</translation>
    </message>
    <message>
        <location filename="qsnshapes.cpp" line="400"/>
        <source>Fire</source>
        <oldsource>Fire alarm</oldsource>
        <translation>Пожар</translation>
    </message>
    <message>
        <location filename="qsnshapes.cpp" line="401"/>
        <source>Stopped fire</source>
        <translation>Прекращение пожара</translation>
    </message>
    <message>
        <location filename="qsnshapes.cpp" line="402"/>
        <source>Water leakage</source>
        <translation>Утечка воды</translation>
    </message>
    <message>
        <location filename="qsnshapes.cpp" line="1098"/>
        <source>Mo</source>
        <translation>Пн</translation>
    </message>
    <message>
        <location filename="qsnshapes.cpp" line="1099"/>
        <source>To</source>
        <translation>Вт</translation>
    </message>
    <message>
        <location filename="qsnshapes.cpp" line="1100"/>
        <source>We</source>
        <translation>Ср</translation>
    </message>
    <message>
        <location filename="qsnshapes.cpp" line="1101"/>
        <source>Th</source>
        <translation>Чт</translation>
    </message>
    <message>
        <location filename="qsnshapes.cpp" line="1102"/>
        <source>Fr</source>
        <translation>Пт</translation>
    </message>
    <message>
        <location filename="qsnshapes.cpp" line="1103"/>
        <source>Sa</source>
        <translation>Сб</translation>
    </message>
    <message>
        <location filename="qsnshapes.cpp" line="1104"/>
        <source>Su</source>
        <translation>Вс</translation>
    </message>
    <message>
        <location filename="qsnshapes.cpp" line="1105"/>
        <source>none</source>
        <translation>Ничего</translation>
    </message>
    <message>
        <location filename="qsnshapes.cpp" line="406"/>
        <source>Low oxygen levels</source>
        <translation>Низкий уровень кислорода</translation>
    </message>
    <message>
        <location filename="qsnshapes.cpp" line="1018"/>
        <source>Silent mode</source>
        <translation>Безывучный режим</translation>
    </message>
    <message>
        <location filename="qsnshapes.cpp" line="1020"/>
        <source>The signal sets the device to silent mode.</source>
        <translation>Сигнал переводит устройства в беззвучный режим.</translation>
    </message>
    <message>
        <location filename="qsnshapes.cpp" line="417"/>
        <source>Digit 0</source>
        <translation>Цифра 0</translation>
    </message>
    <message>
        <location filename="qsnshapes.cpp" line="10"/>
        <source>BOOL</source>
        <translation>Двоичный</translation>
    </message>
    <message>
        <location filename="qsnshapes.cpp" line="11"/>
        <location filename="qsnshapes.cpp" line="13"/>
        <location filename="qsnshapes.cpp" line="153"/>
        <source>State</source>
        <translation>Состояние</translation>
    </message>
    <message>
        <location filename="qsnshapes.cpp" line="12"/>
        <source>Answer</source>
        <translation>Ответ</translation>
    </message>
    <message>
        <location filename="qsnshapes.cpp" line="14"/>
        <source>uInt8</source>
        <translation></translation>
    </message>
    <message>
        <location filename="qsnshapes.cpp" line="15"/>
        <source>Int8</source>
        <translation></translation>
    </message>
    <message>
        <location filename="qsnshapes.cpp" line="16"/>
        <source>uInt16</source>
        <translation></translation>
    </message>
    <message>
        <location filename="qsnshapes.cpp" line="17"/>
        <source>Int16</source>
        <translation></translation>
    </message>
    <message>
        <location filename="qsnshapes.cpp" line="18"/>
        <location filename="qsnshapes.cpp" line="155"/>
        <source>Temperature</source>
        <translation>Температура</translation>
    </message>
    <message>
        <location filename="qsnshapes.cpp" line="19"/>
        <source>Power requirements</source>
        <translation>Потребляемая мощность</translation>
    </message>
    <message>
        <location filename="qsnshapes.cpp" line="20"/>
        <location filename="qsnshapes.cpp" line="157"/>
        <source>Current</source>
        <translation>Ток</translation>
    </message>
    <message>
        <location filename="qsnshapes.cpp" line="21"/>
        <location filename="qsnshapes.cpp" line="158"/>
        <source>Voltage</source>
        <translation>Напряжение</translation>
    </message>
    <message>
        <location filename="qsnshapes.cpp" line="22"/>
        <location filename="qsnshapes.cpp" line="106"/>
        <source>Date and time</source>
        <translation>Дата и время</translation>
    </message>
    <message>
        <location filename="qsnshapes.cpp" line="23"/>
        <location filename="qsnshapes.cpp" line="107"/>
        <location filename="qsnshapes.cpp" line="160"/>
        <location filename="qsnshapes.cpp" line="994"/>
        <source>Alert</source>
        <translation>Оповещение</translation>
    </message>
    <message>
        <location filename="qsnshapes.cpp" line="24"/>
        <location filename="qsnshapes.cpp" line="108"/>
        <location filename="qsnshapes.cpp" line="161"/>
        <source>Error</source>
        <translation>Ошибка</translation>
    </message>
    <message>
        <location filename="qsnshapes.cpp" line="25"/>
        <location filename="qsnshapes.cpp" line="109"/>
        <location filename="qsnshapes.cpp" line="162"/>
        <source>Text</source>
        <translation>Текст</translation>
    </message>
    <message>
        <location filename="qsnshapes.cpp" line="26"/>
        <location filename="qsnshapes.cpp" line="110"/>
        <location filename="qsnshapes.cpp" line="163"/>
        <source>Date</source>
        <translation>Дата</translation>
    </message>
    <message>
        <location filename="qsnshapes.cpp" line="27"/>
        <location filename="qsnshapes.cpp" line="111"/>
        <location filename="qsnshapes.cpp" line="159"/>
        <location filename="qsnshapes.cpp" line="164"/>
        <location filename="qsnshapes.cpp" line="1000"/>
        <source>Time</source>
        <translation>Время</translation>
    </message>
    <message>
        <location filename="qsnshapes.cpp" line="28"/>
        <location filename="qsnshapes.cpp" line="112"/>
        <source>Energy consumed</source>
        <translation>Потребленная энергия</translation>
    </message>
    <message>
        <location filename="qsnshapes.cpp" line="29"/>
        <location filename="qsnshapes.cpp" line="166"/>
        <source>Humidity</source>
        <translation>Влажность</translation>
    </message>
    <message>
        <location filename="qsnshapes.cpp" line="30"/>
        <location filename="qsnshapes.cpp" line="167"/>
        <source>Pressure</source>
        <translation>Давление</translation>
    </message>
    <message>
        <location filename="qsnshapes.cpp" line="31"/>
        <location filename="qsnshapes.cpp" line="168"/>
        <source>Level</source>
        <translation>Уровень</translation>
    </message>
    <message>
        <location filename="qsnshapes.cpp" line="32"/>
        <location filename="qsnshapes.cpp" line="169"/>
        <source>Distance</source>
        <translation>Расстояние</translation>
    </message>
    <message>
        <location filename="qsnshapes.cpp" line="33"/>
        <location filename="qsnshapes.cpp" line="170"/>
        <source>Angle</source>
        <translation>Угол</translation>
    </message>
    <message>
        <location filename="qsnshapes.cpp" line="34"/>
        <location filename="qsnshapes.cpp" line="171"/>
        <source>Illumination</source>
        <translation>Освещенность</translation>
    </message>
    <message>
        <location filename="qsnshapes.cpp" line="35"/>
        <source>uInt32</source>
        <translation>uInt32</translation>
    </message>
    <message>
        <location filename="qsnshapes.cpp" line="36"/>
        <source>Int32</source>
        <translation></translation>
    </message>
    <message>
        <location filename="qsnshapes.cpp" line="37"/>
        <location filename="qsnshapes.cpp" line="174"/>
        <source>Capacity</source>
        <translation>Объем</translation>
    </message>
    <message>
        <location filename="qsnshapes.cpp" line="38"/>
        <location filename="qsnshapes.cpp" line="175"/>
        <source>Weight</source>
        <translation>Вес</translation>
    </message>
    <message>
        <location filename="qsnshapes.cpp" line="39"/>
        <source>Hex code</source>
        <translation>HEX код</translation>
    </message>
    <message>
        <location filename="qsnshapes.cpp" line="41"/>
        <location filename="qsnshapes.cpp" line="125"/>
        <source>SNIR package</source>
        <translation>Пакет SNIR</translation>
    </message>
    <message>
        <location filename="qsnshapes.cpp" line="42"/>
        <location filename="qsnshapes.cpp" line="126"/>
        <source>EM-Marine UID</source>
        <translation>EM-Marine UID</translation>
    </message>
    <message>
        <location filename="qsnshapes.cpp" line="43"/>
        <location filename="qsnshapes.cpp" line="180"/>
        <source>Variant</source>
        <translation>Вариант</translation>
    </message>
    <message>
        <location filename="qsnshapes.cpp" line="93"/>
        <source>None</source>
        <translation>никакой</translation>
    </message>
    <message>
        <location filename="qsnshapes.cpp" line="94"/>
        <source>TRUE/FALSE</source>
        <translation>ИСТИНА/ЛОЖЬ</translation>
    </message>
    <message>
        <location filename="qsnshapes.cpp" line="95"/>
        <source>ON/OFF</source>
        <translation>ВКЛ/ВЫКЛ</translation>
    </message>
    <message>
        <location filename="qsnshapes.cpp" line="96"/>
        <source>YES/NO</source>
        <translation>ДА/НЕТ</translation>
    </message>
    <message>
        <location filename="qsnshapes.cpp" line="97"/>
        <source>OPEN/CLOSE</source>
        <translation>ОТКРЫТО/ЗАКРЫТО</translation>
    </message>
    <message>
        <location filename="qsnshapes.cpp" line="98"/>
        <source>uInt8 0...255</source>
        <translation></translation>
    </message>
    <message>
        <location filename="qsnshapes.cpp" line="99"/>
        <source>Int8 -127...127</source>
        <translation></translation>
    </message>
    <message>
        <location filename="qsnshapes.cpp" line="100"/>
        <source>uInt16 0...65535</source>
        <translation></translation>
    </message>
    <message>
        <location filename="qsnshapes.cpp" line="101"/>
        <source>Int16 -32767…32767</source>
        <translation></translation>
    </message>
    <message>
        <location filename="qsnshapes.cpp" line="102"/>
        <source>Temperature -127.99..+128 C</source>
        <translation>Температура -127.99..+128 C</translation>
    </message>
    <message>
        <location filename="qsnshapes.cpp" line="103"/>
        <source>Power -2147483647…+2147483647 mW</source>
        <translation>Мощность -2147483647…+2147483647 мВт</translation>
    </message>
    <message>
        <location filename="qsnshapes.cpp" line="104"/>
        <source>Current -2147483647…+2147483647 mA</source>
        <translation>Ток -2147483647…+2147483647 мА</translation>
    </message>
    <message>
        <location filename="qsnshapes.cpp" line="105"/>
        <source>Voltage -2147483647…+2147483647 mV</source>
        <translation>Напряжение -2147483647…+2147483647 мВ</translation>
    </message>
    <message>
        <location filename="qsnshapes.cpp" line="113"/>
        <source>Humidity 0..100%</source>
        <translation>Влажность 0..100%</translation>
    </message>
    <message>
        <location filename="qsnshapes.cpp" line="114"/>
        <source>Pressure 0...65535 hPa</source>
        <translation>Давление 0...65535 гПа</translation>
    </message>
    <message>
        <location filename="qsnshapes.cpp" line="115"/>
        <source>Level 0..100%</source>
        <translation>Уровень 0..100%</translation>
    </message>
    <message>
        <location filename="qsnshapes.cpp" line="116"/>
        <source>Distance 0...65535 mm</source>
        <translation>Растояние 0...65535 мм</translation>
    </message>
    <message>
        <location filename="qsnshapes.cpp" line="117"/>
        <source>Angle -21600…21600 minutus</source>
        <translation>Угол -21600…21600 минут</translation>
    </message>
    <message>
        <location filename="qsnshapes.cpp" line="118"/>
        <source>Lux 0...65535 lx</source>
        <translation>Освещенность 0...65535 лк</translation>
    </message>
    <message>
        <location filename="qsnshapes.cpp" line="119"/>
        <source>uInt32 0...4294967296</source>
        <translation></translation>
    </message>
    <message>
        <location filename="qsnshapes.cpp" line="120"/>
        <source>Int32 -2147483647…+2147483647</source>
        <translation></translation>
    </message>
    <message>
        <location filename="qsnshapes.cpp" line="121"/>
        <source>Capacity 0...4294967296 ml</source>
        <translation>Вместимость 0 ... 4294967296 мл</translation>
    </message>
    <message>
        <location filename="qsnshapes.cpp" line="122"/>
        <source>Weight 0...4294967296 mg</source>
        <translation>Вес 0 ... 4294967296 мг</translation>
    </message>
    <message>
        <location filename="qsnshapes.cpp" line="135"/>
        <source>F</source>
        <translation></translation>
    </message>
    <message>
        <location filename="qsnshapes.cpp" line="136"/>
        <source>Wt</source>
        <translation>Вт</translation>
    </message>
    <message>
        <location filename="qsnshapes.cpp" line="137"/>
        <source>A</source>
        <translation>А</translation>
    </message>
    <message>
        <location filename="qsnshapes.cpp" line="138"/>
        <source>V</source>
        <translation>В</translation>
    </message>
    <message>
        <location filename="qsnshapes.cpp" line="154"/>
        <location filename="qsnshapes.cpp" line="172"/>
        <location filename="qsnshapes.cpp" line="173"/>
        <source>Number</source>
        <translation>Число</translation>
    </message>
    <message>
        <location filename="qsnshapes.cpp" line="156"/>
        <source>Power</source>
        <translation>Мощность</translation>
    </message>
    <message>
        <location filename="qsnshapes.cpp" line="403"/>
        <source>Stopped water leakage</source>
        <translation>Прекращение утечки воды</translation>
    </message>
    <message>
        <location filename="qsnshapes.cpp" line="404"/>
        <source>Is absent electricity supply</source>
        <translation>Отсутствие электроснабжения</translation>
    </message>
    <message>
        <location filename="qsnshapes.cpp" line="405"/>
        <source>Electricity supply restored</source>
        <translation>Электроснабжение востановленно</translation>
    </message>
    <message>
        <location filename="qsnshapes.cpp" line="407"/>
        <source>Oxygen levels are normal</source>
        <translation>Уровень кислорода в норме</translation>
    </message>
    <message>
        <location filename="qsnshapes.cpp" line="409"/>
        <source>Gas leak is absent</source>
        <translation>Утечка газа прекращена</translation>
    </message>
    <message>
        <location filename="qsnshapes.cpp" line="418"/>
        <source>Digit 1</source>
        <translation>Цифра 1</translation>
    </message>
    <message>
        <location filename="qsnshapes.cpp" line="419"/>
        <source>Digit 2</source>
        <translation>Цифра 2</translation>
    </message>
    <message>
        <location filename="qsnshapes.cpp" line="420"/>
        <source>Digit 3</source>
        <translation>Цифра 3</translation>
    </message>
    <message>
        <location filename="qsnshapes.cpp" line="421"/>
        <source>Digit 4</source>
        <translation>Цифра 4</translation>
    </message>
    <message>
        <location filename="qsnshapes.cpp" line="422"/>
        <source>Digit 5</source>
        <translation>Цифра 5</translation>
    </message>
    <message>
        <location filename="qsnshapes.cpp" line="423"/>
        <source>Digit 6</source>
        <translation>Цифра 6</translation>
    </message>
    <message>
        <location filename="qsnshapes.cpp" line="424"/>
        <source>Digit 7</source>
        <translation>Цифра 7</translation>
    </message>
    <message>
        <location filename="qsnshapes.cpp" line="425"/>
        <source>Digit 8</source>
        <translation>Цифра 8</translation>
    </message>
    <message>
        <location filename="qsnshapes.cpp" line="426"/>
        <source>Digit 9</source>
        <translation>Цифра 9</translation>
    </message>
    <message>
        <location filename="qsnshapes.cpp" line="427"/>
        <source>Arming</source>
        <translation>Постановка на охрану</translation>
    </message>
    <message>
        <location filename="qsnshapes.cpp" line="428"/>
        <source>Disarming</source>
        <translation>Снятие с охраны</translation>
    </message>
    <message>
        <location filename="qsnshapes.cpp" line="429"/>
        <source>Penetration</source>
        <translation>Проникновение</translation>
    </message>
    <message>
        <location filename="qsnshapes.cpp" line="430"/>
        <source>Cancel the alarm</source>
        <translation>Отмена сигнала тревоги</translation>
    </message>
    <message>
        <location filename="qsnshapes.cpp" line="431"/>
        <source>Arming (secretly)</source>
        <oldsource>Arming (quiet)</oldsource>
        <translation>Постановка (тайно)</translation>
    </message>
    <message>
        <location filename="qsnshapes.cpp" line="946"/>
        <source>Departure status</source>
        <translation></translation>
    </message>
    <message>
        <location filename="qsnshapes.cpp" line="948"/>
        <source>The signal informs all devices on the long-term absence</source>
        <translation></translation>
    </message>
    <message>
        <location filename="qsnshapes.cpp" line="952"/>
        <source>Presence status</source>
        <translation></translation>
    </message>
    <message>
        <location filename="qsnshapes.cpp" line="954"/>
        <source>The signal notifies the device presence.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="qsnshapes.cpp" line="958"/>
        <source>Absence status</source>
        <translation></translation>
    </message>
    <message>
        <location filename="qsnshapes.cpp" line="960"/>
        <source>The signal notifies the device of the absence.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="qsnshapes.cpp" line="964"/>
        <source>Nighttime</source>
        <translation></translation>
    </message>
    <message>
        <location filename="qsnshapes.cpp" line="966"/>
        <source>The signal notifies the device of night time.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="qsnshapes.cpp" line="970"/>
        <source>No nighttime</source>
        <translation></translation>
    </message>
    <message>
        <location filename="qsnshapes.cpp" line="972"/>
        <source>The signal notifies the device is not on a night time.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="qsnshapes.cpp" line="976"/>
        <source>Daytime</source>
        <translation></translation>
    </message>
    <message>
        <location filename="qsnshapes.cpp" line="978"/>
        <source>The signal notifies the device of day time.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="qsnshapes.cpp" line="982"/>
        <source>Not daytime</source>
        <translation></translation>
    </message>
    <message>
        <location filename="qsnshapes.cpp" line="984"/>
        <source>The signal notifies the device is not on a day time.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="qsnshapes.cpp" line="996"/>
        <source>A signal for notifying occurrence of an alarm event.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="qsnshapes.cpp" line="1008"/>
        <source>A signal for security occurrence of an security alarm event.</source>
        <translation></translation>
    </message>
    <message>
        <location filename="qsnshapes.cpp" line="1014"/>
        <source>A signal notifies the device of bell event.</source>
        <translation>Сигнал уведомляет устройства о наступлении события звонок.</translation>
    </message>
    <message>
        <location filename="qsnshapes.cpp" line="40"/>
        <location filename="qsnshapes.cpp" line="124"/>
        <location filename="qsnshapes.cpp" line="177"/>
        <location filename="qsnshapes.cpp" line="1006"/>
        <source>Security alarm</source>
        <translation>Охранная сигнализация</translation>
    </message>
    <message>
        <location filename="qsnshapes.cpp" line="408"/>
        <source>Gas leak</source>
        <translation>Утечка газа</translation>
    </message>
    <message>
        <location filename="qsnshapes.cpp" line="1012"/>
        <source>Bell</source>
        <translation>Звонок</translation>
    </message>
    <message>
        <location filename="qsnshapes.cpp" line="940"/>
        <source>No type</source>
        <translation>Без типа</translation>
    </message>
    <message>
        <location filename="qsnshapes.cpp" line="942"/>
        <source>Description channel</source>
        <translation>Описание канала</translation>
    </message>
    <message>
        <location filename="qsnshapes.cpp" line="988"/>
        <source>Errors</source>
        <translation>Ошибки</translation>
    </message>
    <message>
        <location filename="qsnshapes.cpp" line="990"/>
        <source>Channel errors, transmits the address and the error code.</source>
        <translation>Канал ошибок, передает адрес и код ошибки устройства.</translation>
    </message>
    <message>
        <location filename="qsnshapes.cpp" line="1002"/>
        <source>The time signal, the device transmits to the clock or the device receives the time signal.</source>
        <translation>Сигнал времени, используется для передачи точного времени.</translation>
    </message>
</context>
<context>
    <name>QsnInterface</name>
    <message>
        <location filename="qsninterface.cpp" line="107"/>
        <source>Address changed from %1 to %2</source>
        <translation>Адрес изменился с %1 на %2</translation>
    </message>
    <message>
        <location filename="qsninterface.cpp" line="218"/>
        <source>Device</source>
        <translation>Устройство</translation>
    </message>
    <message>
        <location filename="qsninterface.cpp" line="218"/>
        <source>Address</source>
        <translation>Адрес</translation>
    </message>
</context>
<context>
    <name>QsnTCPclient</name>
    <message>
        <location filename="qsntcpclient.cpp" line="7"/>
        <source>TCPclient</source>
        <translation>TCP клиент</translation>
    </message>
    <message>
        <location filename="qsntcpclient.cpp" line="66"/>
        <source>Connect to server successfully</source>
        <translation>Успешное подключение к серверу</translation>
    </message>
    <message>
        <location filename="qsntcpclient.cpp" line="68"/>
        <source>Authorization failed</source>
        <translation>Авторизация не удалась</translation>
    </message>
    <message>
        <location filename="qsntcpclient.cpp" line="118"/>
        <source>Connect to server failed</source>
        <translation>Подключение к серверу не удалось</translation>
    </message>
    <message>
        <location filename="qsntcpclient.cpp" line="236"/>
        <source>The connection terminated unexpectedly!</source>
        <translation>Соединение неожиданно разорвано!</translation>
    </message>
</context>
<context>
    <name>QsnUDPclient</name>
    <message>
        <location filename="qsnudpclient.cpp" line="7"/>
        <source>UDP Client</source>
        <translation>UDP Клиент</translation>
    </message>
    <message>
        <location filename="qsnudpclient.cpp" line="69"/>
        <source>Bind failed</source>
        <translation>Не удачное открытие порта на прослушивание</translation>
    </message>
    <message>
        <location filename="qsnudpclient.cpp" line="72"/>
        <source>Bind port %1</source>
        <translation>Открыт порт на прослушивание %1</translation>
    </message>
    <message>
        <location filename="qsnudpclient.cpp" line="163"/>
        <location filename="qsnudpclient.cpp" line="164"/>
        <source>Close</source>
        <translation>Закрыт</translation>
    </message>
    <message>
        <location filename="qsnudpclient.cpp" line="187"/>
        <location filename="qsnudpclient.cpp" line="188"/>
        <source>Registration failed</source>
        <translation>Регистрация не удалась</translation>
    </message>
    <message>
        <location filename="qsnudpclient.cpp" line="199"/>
        <location filename="qsnudpclient.cpp" line="200"/>
        <source>Registration is successful</source>
        <translation>Регистрация прошла успешно</translation>
    </message>
    <message>
        <location filename="qsnudpclient.cpp" line="206"/>
        <location filename="qsnudpclient.cpp" line="207"/>
        <source>Registration is denied</source>
        <translation>Отказано в регистрации</translation>
    </message>
</context>
</TS>

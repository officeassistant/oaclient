#include "qsnmediakey.h"

#ifdef Q_OS_WIN
#include <Windows.h>
#endif

#ifdef Q_OS_LINUX
#include <X11/Xlib.h>
#include <X11/extensions/XTest.h>
#define XF86AudioLowerVolume            0x1008ff11
#define XF86AudioMute                   0x1008ff12
#define XF86AudioRaiseVolume            0x1008ff13
#define XF86AudioPlay                   0x1008ff14
#define XF86AudioStop                   0x1008ff15
#define XF86AudioPrev                   0x1008ff16
#define XF86AudioNext                   0x1008ff17
#define XF86AudioPause                  0x1008ff31
#define XF86XK_Back                     0x1008FF26
#define XF86XK_HomePage                 0x1008FF18
#define XF86XK_PowerOff                 0x1008FF2A
#define XK_F                            0x0046
#define XK_Escape                       0xff1b
#define XK_Page_Up                      0xff55
#define XK_Page_Down                    0xff56
#define XK_End                          0xff57
#define XK_Left                         0xff51
#define XK_Up                           0xff52
#define XK_Right                        0xff53
#define XK_Down                         0xff54
#define XK_Prior                        0xff55
#define XK_Next                         0xff56
#define XK_Select                       0xff60
#define XK_Return                       0xff0d
#define XK_Alt_R                        0xffea
#define XF86XK_PowerOff                 0x1008FF2A
#define XF86XK_Hibernate                0x1008FFA8
#define XF86XK_LogOff                   0x1008FF61
#define XF86XK_ScreenSaver              0x1008FF2D
#define XK_0                            0x0030
#define XK_1                            0x0031
#define XK_2                            0x0032
#define XK_3                            0x0033
#define XK_4                            0x0034
#define XK_5                            0x0035
#define XK_6                            0x0036
#define XK_7                            0x0037
#define XK_8                            0x0038
#define XK_9                            0x0039
#define XK_Tab                          0xff09
#define XK_F4                           0xffc1
#define XK_Space                        0x0020
#define XK_Control                      0x0011
#endif

QsnMediaKey::QsnMediaKey(QObject *parent) : QObject(parent)
{

}

QVariant QsnMediaKey::qkeyToOSkey(Qt::Key qtKey)
{
#ifdef Q_OS_LINUX
    switch (qtKey) {
    case Qt::Key_MediaPrevious: return XF86AudioPrev;
    case Qt::Key_MediaTogglePlayPause: return XF86AudioPlay;
    case Qt::Key_MediaNext: return XF86AudioNext;
    case Qt::Key_VolumeDown: return XF86AudioLowerVolume;
    case Qt::Key_VolumeMute: return XF86AudioMute;
    case Qt::Key_VolumeUp: return XF86AudioRaiseVolume;
    case Qt::Key_MediaPlay: return XF86AudioPlay;
    case Qt::Key_MediaStop: return XF86AudioStop;
    case Qt::Key_MediaPause: return XF86AudioPause;
    case Qt::Key_Back: return XF86XK_Back;
    case Qt::Key_Home: return XF86XK_HomePage;
    case Qt::Key_F: return XK_F;
    case Qt::Key_Escape: return XK_Escape;
    case Qt::Key_PageUp: return XK_Page_Up;
    case Qt::Key_PageDown: return XK_Page_Down;
    case Qt::Key_End: return XK_End;
    case Qt::Key_Left: return XK_Left;
    case Qt::Key_Up: return XK_Up;
    case Qt::Key_Right: return XK_Right;
    case Qt::Key_Down: return XK_Down;
    case Qt::Key_Select: return XK_Select;
    case Qt::Key_Enter: return XK_Return;
    case Qt::Key_Alt: return XK_Alt_R;
    case Qt::Key_PowerOff: return XF86XK_PowerOff;
    case Qt::Key_Hibernate: return XF86XK_Hibernate;
    case Qt::Key_LogOff: return XF86XK_LogOff;
    case Qt::Key_ScreenSaver: return XF86XK_ScreenSaver;
    case Qt::Key_0: return XK_0;
    case Qt::Key_1: return XK_1;
    case Qt::Key_2: return XK_2;
    case Qt::Key_3: return XK_3;
    case Qt::Key_4: return XK_4;
    case Qt::Key_5: return XK_5;
    case Qt::Key_6: return XK_6;
    case Qt::Key_7: return XK_7;
    case Qt::Key_8: return XK_8;
    case Qt::Key_9: return XK_9;
    case Qt::Key_Tab: return XK_Tab;
    case Qt::Key_F4: return XK_F4;
    case Qt::Key_Space: return XK_Space;
    case Qt::Key_Control: return VK_Control;
    default: return 0;
    }
#endif
#ifdef Q_OS_WIN
    switch (qtKey) {
    case Qt::Key_MediaPrevious: return VK_MEDIA_PREV_TRACK;
    case Qt::Key_MediaTogglePlayPause: return VK_MEDIA_PLAY_PAUSE;
    case Qt::Key_MediaNext: return VK_MEDIA_NEXT_TRACK;
    case Qt::Key_VolumeDown: return VK_VOLUME_DOWN;
    case Qt::Key_VolumeMute: return VK_VOLUME_MUTE;
    case Qt::Key_VolumeUp: return VK_VOLUME_UP;
    case Qt::Key_MediaPlay: return VK_MEDIA_PLAY_PAUSE;
    case Qt::Key_MediaStop: return VK_MEDIA_STOP;
    case Qt::Key_MediaPause: return VK_PAUSE;
    case Qt::Key_Back: return VK_BACK;
    case Qt::Key_Escape: return VK_ESCAPE;
    case Qt::Key_PageUp: return VK_PRIOR;
    case Qt::Key_PageDown: return VK_NEXT;
    case Qt::Key_End: return VK_END;
    case Qt::Key_Home: return VK_HOME;
    case Qt::Key_Left: return VK_LEFT;
    case Qt::Key_Up: return VK_UP;
    case Qt::Key_Right: return VK_RIGHT;
    case Qt::Key_Down: return VK_DOWN;
    case Qt::Key_Tab: return VK_TAB;
    case Qt::Key_Enter: return VK_RETURN;
    case Qt::Key_Alt: return VK_MENU;
    case Qt::Key_F4: return VK_F4;
    case Qt::Key_0: return VK_NUMPAD0;
    case Qt::Key_1: return VK_NUMPAD1;
    case Qt::Key_2: return VK_NUMPAD2;
    case Qt::Key_3: return VK_NUMPAD3;
    case Qt::Key_4: return VK_NUMPAD4;
    case Qt::Key_5: return VK_NUMPAD5;
    case Qt::Key_6: return VK_NUMPAD6;
    case Qt::Key_7: return VK_NUMPAD7;
    case Qt::Key_8: return VK_NUMPAD8;
    case Qt::Key_9: return VK_NUMPAD9;
    case Qt::Key_Space: return VK_SPACE;
    case Qt::Key_Control: return VK_CONTROL;
    default: return 0;
    }
#endif
    return 0;
}

void QsnMediaKey::sendKeyEventToSystem(Qt::Key qtKey)
{
#ifdef Q_OS_WIN
    INPUT ip;
    ip.type = INPUT_KEYBOARD;
    ip.ki.wScan = 0;
    ip.ki.time = 0;
    ip.ki.dwExtraInfo = 0;
    ip.ki.wVk = static_cast<quint16>(qkeyToOSkey(qtKey).toUInt());
    ip.ki.dwFlags = 0;
    if (ip.ki.wVk == 0) return;
    SendInput(1, &ip, sizeof(INPUT));
    ip.ki.dwFlags = KEYEVENTF_KEYUP;
    SendInput(1, &ip, sizeof(INPUT));

#endif
#ifdef Q_OS_LINUX
    unsigned int key = qkeyToOSkey(qtKey).toInt();
    unsigned int keycode;
    if (key == 0) return;

    Display *display;
    display = XOpenDisplay(NULL);
    keycode = XKeysymToKeycode(display, key);
    XTestFakeKeyEvent(display, keycode, 1, 0);
    XTestFakeKeyEvent(display, keycode, 0, 0);
    XFlush(display);
    XCloseDisplay(display);
#endif
}

void QsnMediaKey::sendTwoKeyEventToSystem(Qt::Key qtKey1, Qt::Key qtKey2)
{
#ifdef Q_OS_WIN
    INPUT ip;
    ip.type = INPUT_KEYBOARD;
    ip.ki.wScan = 0;
    ip.ki.time = 0;
    ip.ki.dwExtraInfo = 0;
    ip.ki.dwFlags = 0;
    ip.ki.wVk = static_cast<quint16>(qkeyToOSkey(qtKey1).toUInt());
    SendInput(1, &ip, sizeof(INPUT));
    ip.ki.dwFlags = 0;
    ip.ki.wVk = static_cast<quint16>(qkeyToOSkey(qtKey2).toUInt());
    SendInput(1, &ip, sizeof(INPUT));
    ip.ki.dwFlags = KEYEVENTF_KEYUP;
    SendInput(1, &ip, sizeof(INPUT));
    ip.ki.wVk = static_cast<quint16>(qkeyToOSkey(qtKey1).toUInt());
    ip.ki.dwFlags = KEYEVENTF_KEYUP;
    SendInput(1, &ip, sizeof(INPUT));
#endif
#ifdef Q_OS_LINUX
    unsigned int key1 = qkeyToOSkey(qtKey1).toInt();
    unsigned int keycode1;
    unsigned int key2 = qkeyToOSkey(qtKey2).toInt();
    unsigned int keycode2;
    if (key1 == 0 || key2 == 0) return;

    Display *display;
    display = XOpenDisplay(NULL);
    keycode1 = XKeysymToKeycode(display, key1);
    keycode2 = XKeysymToKeycode(display, key2);
    XTestFakeKeyEvent(display, keycode1, 1, 0);
    XTestFakeKeyEvent(display, keycode2, 1, 0);
    XTestFakeKeyEvent(display, keycode2, 0, 0);
    XTestFakeKeyEvent(display, keycode1, 0, 0);
    XFlush(display);
    XCloseDisplay(display);
#endif

}

#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    trayIconMenu = new QMenu(this);
    trayIconMenu->addAction(ui->actionTemperature);
#ifdef Q_OS_WIN
    ui->actionTemperature->setVisible(false);
#endif
#ifdef Q_OS_LINUX
    ui->actionTemperature->setEnabled(false);
#endif
    trayIconMenu->addSeparator();
    trayIconMenu->addAction(ui->actionSettings);
    trayIconMenu->addSeparator();
    trayIconMenu->addAction(ui->actionExit);

    oaclient = new QOATCPclient(this);

    trayIcon = new QSystemTrayIcon(this);
    trayIcon->setContextMenu(trayIconMenu);

    absencecount = -2;
    setTrayIcon();

    trayIcon->show();

    disableClose = true;

    connect(trayIcon, SIGNAL(activated(QSystemTrayIcon::ActivationReason)),
            this, SLOT(iconActivated(QSystemTrayIcon::ActivationReason)));
    connect(&checkTimer, SIGNAL(timeout()), this, SLOT(checkPulse()));

    connect(oaclient, SIGNAL(changeState(QString)), this, SLOT(changeOAState(QString)));
    //    connect(udpClient, SIGNAL(snBUSOutput(QSNContainer,QObject*)), snInterface, SLOT(snBUSInput(QSNContainer,QObject*)));
    //    connect(snInterface, SIGNAL(snBUSOutput(QSNContainer,QObject*)), tcpClient, SLOT(snBUSInput(QSNContainer,QObject*)));
    //    connect(snInterface, SIGNAL(snBUSOutput(QSNContainer,QObject*)), udpClient, SLOT(snBUSInput(QSNContainer,QObject*)));
    //    connect(snInterface, SIGNAL(snBUSOutput(QSNContainer,QObject*)), this, SLOT(snBUSInput(QSNContainer,QObject*)));
    //    connect(snInterface, SIGNAL(eventConnect()), this, SLOT(connectUpdate()));
    //    connect(snInterface, SIGNAL(eventDisconnect()), this, SLOT(connectUpdate()));
    //    connect(snInterface, SIGNAL(eventNumInput(int, QByteArray)), this, SLOT(eventNumInput(int, QByteArray)));

    readSettings();
    lastPulse = QDateTime::currentDateTime();
    // setWindowsPosition();

    checkTimer.setInterval(10000);
    checkTimer.start();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::closeEvent(QCloseEvent *event)
{
    if (disableClose) {
        event->ignore();
        hide();
    } else {
        event->accept();
    }
}

void MainWindow::eventNumInput(int numInput, QByteArray Data)
{
    switch (numInput) {
    case input_alert: showAlert(&Data); break;
    case input_alarm: showAlarm(&Data); break;
    case input_bell: showBell(); break;
        //case input_ircode: decodeCommand(QSNRAWtoUInt32(&Data, 1)); break;
    case input_poweroff: poweroff(); break;
    case input_restart: restart(); break;
    case input_logout: logout(); break;
    case input_hibernation: hibernation(); break;
    case input_lock: lock(); break;
    case input_temperature: readTemperature(&Data); break;
    }
}

void MainWindow::iconActivated(QSystemTrayIcon::ActivationReason reason)
{
    switch (reason) {
    case QSystemTrayIcon::Trigger: break;
    case QSystemTrayIcon::DoubleClick:
        if (absencecount > 0 && absencecount < 3) {
            absencecount = -2;
            setTrayIcon();
        }
        break;
    case QSystemTrayIcon::MiddleClick:  break;
    default:  ;
    }
}


void MainWindow::writeSettings()
{
    QSettings settings;
    settings.setValue("geometry", geometry());
    settings.setValue("mac", ui->macEdit->text());
    //    snInterface->saveSettings(&settings);
    oaclient->saveSettings(&settings);
    //    udpClient->saveSettings(&settings);
}

void MainWindow::readSettings()
{
    QSettings settings;
    QRect geom = settings.value("geometry", geometry()).toRect();
    ui->macEdit->setText(settings.value("mac", ui->macEdit->text()).toString());
    if (geom.left() > 0 && geom.top() > 0) setGeometry(geom);
    oaclient->loadSettings(&settings);
    ui->oaPortEdit->setValue(oaclient->getPort());
    ui->oaAddressEdit->setText(oaclient->getAddress());
    ui->tcpSettings->setChecked(oaclient->isEnabled());
    //    ui->udpPortEdit->setValue(udpClient->getPort());
    //    ui->udpAddressEdit->setText(udpClient->getAddress());
    //    ui->udpKeyEdit->setText(udpClient->getKey());
    //    ui->udpSettings->setChecked(udpClient->isEnabled());
}

//void MainWindow::decodeCommand(quint32 code)
//{
//    //    QByteArray *memory = snInterface->getMemory();
//    //    if (QSNRAWtoUInt32(memory, ir_MediaPrevious) == code) mkey.sendKeyEventToSystem(Qt::Key_MediaPrevious);
//    //    if (QSNRAWtoUInt32(memory, ir_MediaNext) == code) mkey.sendKeyEventToSystem(Qt::Key_MediaNext);
//    //    if (QSNRAWtoUInt32(memory, ir_MediaTogglePlayPause) == code) mkey.sendKeyEventToSystem(Qt::Key_MediaTogglePlayPause);
//    //    if (QSNRAWtoUInt32(memory, ir_MediaPlay) == code) mkey.sendKeyEventToSystem(Qt::Key_MediaPlay);
//    //    if (QSNRAWtoUInt32(memory, ir_MediaStop) == code) mkey.sendKeyEventToSystem(Qt::Key_MediaStop);
//    //    if (QSNRAWtoUInt32(memory, ir_MediaPause) == code) mkey.sendKeyEventToSystem(Qt::Key_MediaPause);
//    //    if (QSNRAWtoUInt32(memory, ir_VolumeUp) == code) mkey.sendKeyEventToSystem(Qt::Key_VolumeUp);
//    //    if (QSNRAWtoUInt32(memory, ir_VolumeDown) == code) mkey.sendKeyEventToSystem(Qt::Key_VolumeDown);
//    //    if (QSNRAWtoUInt32(memory, ir_VolumeMute) == code) mkey.sendKeyEventToSystem(Qt::Key_VolumeMute);
//    //    if (QSNRAWtoUInt32(memory, ir_Sleep) == code) mkey.sendKeyEventToSystem(Qt::Key_Sleep);
//    //    if (QSNRAWtoUInt32(memory, ir_Back) == code) mkey.sendKeyEventToSystem(Qt::Key_Back);
//    //    if (QSNRAWtoUInt32(memory, ir_Escape) == code) mkey.sendKeyEventToSystem(Qt::Key_Escape);
//    //    if (QSNRAWtoUInt32(memory, ir_PageUp) == code) mkey.sendKeyEventToSystem(Qt::Key_PageUp);
//    //    if (QSNRAWtoUInt32(memory, ir_PageDown) == code) mkey.sendKeyEventToSystem(Qt::Key_PageDown);
//    //    if (QSNRAWtoUInt32(memory, ir_End) == code) mkey.sendKeyEventToSystem(Qt::Key_End);
//    //    if (QSNRAWtoUInt32(memory, ir_Home) == code) mkey.sendKeyEventToSystem(Qt::Key_Home);
//    //    if (QSNRAWtoUInt32(memory, ir_Left) == code) mkey.sendKeyEventToSystem(Qt::Key_Left);
//    //    if (QSNRAWtoUInt32(memory, ir_Up) == code) mkey.sendKeyEventToSystem(Qt::Key_Up);
//    //    if (QSNRAWtoUInt32(memory, ir_Right) == code) mkey.sendKeyEventToSystem(Qt::Key_Right);
//    //    if (QSNRAWtoUInt32(memory, ir_Down) == code) mkey.sendKeyEventToSystem(Qt::Key_Down);
//    //    if (QSNRAWtoUInt32(memory, ir_SwitchApp) == code) switchApp();
//    //    if (QSNRAWtoUInt32(memory, ir_Fullscreen) == code) fullScreen();
//    //    if (QSNRAWtoUInt32(memory, ir_Enter) == code) mkey.sendKeyEventToSystem(Qt::Key_Enter);
//    //    if (QSNRAWtoUInt32(memory, ir_Poweroff) == code) poweroff();
//    //    if (QSNRAWtoUInt32(memory, ir_Restart) == code) restart();
//    //    if (QSNRAWtoUInt32(memory, ir_logout) == code) logout();
//    //    if (QSNRAWtoUInt32(memory, ir_Hibernation) == code) hibernation();
//    //    if (QSNRAWtoUInt32(memory, ir_Lock) == code) lock();
//    //    if (QSNRAWtoUInt32(memory, ir_0) == code) mkey.sendKeyEventToSystem(Qt::Key_0);
//    //    if (QSNRAWtoUInt32(memory, ir_1) == code) mkey.sendKeyEventToSystem(Qt::Key_1);
//    //    if (QSNRAWtoUInt32(memory, ir_2) == code) mkey.sendKeyEventToSystem(Qt::Key_2);
//    //    if (QSNRAWtoUInt32(memory, ir_3) == code) mkey.sendKeyEventToSystem(Qt::Key_3);
//    //    if (QSNRAWtoUInt32(memory, ir_4) == code) mkey.sendKeyEventToSystem(Qt::Key_4);
//    //    if (QSNRAWtoUInt32(memory, ir_5) == code) mkey.sendKeyEventToSystem(Qt::Key_5);
//    //    if (QSNRAWtoUInt32(memory, ir_6) == code) mkey.sendKeyEventToSystem(Qt::Key_6);
//    //    if (QSNRAWtoUInt32(memory, ir_7) == code) mkey.sendKeyEventToSystem(Qt::Key_7);
//    //    if (QSNRAWtoUInt32(memory, ir_8) == code) mkey.sendKeyEventToSystem(Qt::Key_8);
//    //    if (QSNRAWtoUInt32(memory, ir_9) == code) mkey.sendKeyEventToSystem(Qt::Key_9);
//    //    if (QSNRAWtoUInt32(memory, ir_Close) == code) appClose();
//    //    if (QSNRAWtoUInt32(memory, ir_space) == code) appClose();

//}

void MainWindow::poweroff()
{
#ifdef Q_OS_WIN
    QProcess::startDetached("shutdown -s -t 00");
#endif
#ifdef Q_OS_LINUX
    mkey.sendKeyEventToSystem(Qt::Key_PowerOff);
#endif
}

void MainWindow::logout()
{
#ifdef Q_OS_WIN
    QProcess::startDetached("shutdown -i -t 00");
#endif
#ifdef Q_OS_LINUX
    mkey.sendKeyEventToSystem(Qt::Key_LogOff);
#endif
}

void MainWindow::hibernation()
{
#ifdef Q_OS_WIN
    QProcess::startDetached("shutdown -h -t 00");
#endif
#ifdef Q_OS_LINUX
    mkey.sendKeyEventToSystem(Qt::Key_Hibernate);
#endif
}

void MainWindow::restart()
{
#ifdef Q_OS_WIN
    QProcess::startDetached("shutdown -g -t 00");
#endif
#ifdef Q_OS_LINUX
    QProcess::startDetached("reboot");
#endif
}

void MainWindow::lock()
{
#ifdef Q_OS_WIN
    QProcess::startDetached("RunDll32.exe user32.dll,LockWorkStation");
#endif
#ifdef Q_OS_LINUX
    mkey.sendKeyEventToSystem(Qt::Key_ScreenSaver);
#endif
}

void MainWindow::fullScreen()
{
#ifdef Q_OS_WIN
    mkey.sendTwoKeyEventToSystem(Qt::Key_Alt, Qt::Key_Enter);
#endif
#ifdef Q_OS_LINUX
    mkey.sendKeyEventToSystem(Qt::Key_F);
#endif
}

void MainWindow::appClose()
{
    mkey.sendTwoKeyEventToSystem(Qt::Key_Alt, Qt::Key_F4);
}

void MainWindow::switchApp()
{
    mkey.sendTwoKeyEventToSystem(Qt::Key_Alt, Qt::Key_Tab);
}

void MainWindow::showAlert(QByteArray *data)
{
    if (data->at(0) != 14) return;
    QSNNotification n = QSNRAWtoNotification(data, 1);
#ifdef Q_OS_WIN
    switch (n.notificationType) {
    case 0: trayIcon->showMessage("SignalNet", QSNAlertTypeLabel(n.notificationType), QIcon(":/ico/pOk.png")); break;
    case 1: trayIcon->showMessage("SignalNet", QSNAlertTypeLabel(n.notificationType), QIcon(":/ico/pFire.png"), 60000); break;
    case 2: trayIcon->showMessage("SignalNet", QSNAlertTypeLabel(n.notificationType), QIcon(":/ico/pFireG.png")); break;
    case 3: trayIcon->showMessage("SignalNet", QSNAlertTypeLabel(n.notificationType), QIcon(":/ico/pWater.png"), 60000); break;
    case 4: trayIcon->showMessage("SignalNet", QSNAlertTypeLabel(n.notificationType), QIcon(":/ico/pWaterG.png")); break;
    case 5: trayIcon->showMessage("SignalNet", QSNAlertTypeLabel(n.notificationType), QIcon(":/ico/pPower.png"), 60000); break;
    case 6: trayIcon->showMessage("SignalNet", QSNAlertTypeLabel(n.notificationType), QIcon(":/ico/pPowerG.png")); break;
    case 7: trayIcon->showMessage("SignalNet", QSNAlertTypeLabel(n.notificationType), QIcon(":/ico/pOxygen.png"), 60000); break;
    case 8: trayIcon->showMessage("SignalNet", QSNAlertTypeLabel(n.notificationType), QIcon(":/ico/pOxygenG.png")); break;
    case 9: trayIcon->showMessage("SignalNet", QSNAlertTypeLabel(n.notificationType), QIcon(":/ico/pGas.png"), 60000); break;
    case 10: trayIcon->showMessage("SignalNet", QSNAlertTypeLabel(n.notificationType), QIcon(":/ico/pGasG.png")); break;
    }
#endif
#ifdef Q_OS_LINUX
    if (n.notificationType % 2 == 0)
        trayIcon->showMessage("SignalNet", QSNAlertTypeLabel(n.notificationType), QSystemTrayIcon::Information);
    else
        trayIcon->showMessage("SignalNet", QSNAlertTypeLabel(n.notificationType), QSystemTrayIcon::Warning);
#endif
}

void MainWindow::showAlarm(QByteArray *data)
{
    if (data->at(0) != 31) return;
    QSNNotification n = QSNRAWtoNotification(data, 1);
#ifdef Q_OS_WIN
    switch (n.notificationType) {
    case 10: trayIcon->showMessage("SignalNet", QSNSecurityAlertTypeLabel(n.notificationType), QIcon(":/ico/pLock.png")); break;
    case 11: trayIcon->showMessage("SignalNet", QSNSecurityAlertTypeLabel(n.notificationType), QIcon(":/ico/pUnlock.png")); break;
    case 12: trayIcon->showMessage("SignalNet", QSNSecurityAlertTypeLabel(n.notificationType), QIcon(":/ico/pPenetration.png")); break;
    case 14: trayIcon->showMessage("SignalNet", QSNSecurityAlertTypeLabel(n.notificationType), QIcon(":/ico/pLockSecretly.png")); break;
    }
#endif
#ifdef Q_OS_LINUX
    if (n.notificationType < 10) return;
    if (n.notificationType == 12)
        trayIcon->showMessage("SignalNet", QSNSecurityAlertTypeLabel(n.notificationType), QSystemTrayIcon::Warning);
    else
        trayIcon->showMessage("SignalNet", QSNSecurityAlertTypeLabel(n.notificationType), QSystemTrayIcon::Information);
#endif
}

void MainWindow::showBell()
{
#ifdef Q_OS_WIN
    trayIcon->showMessage("SignalNet", tr("Bell"), QIcon(":/ico/pBell.png"));
#endif
#ifdef Q_OS_LINUX
    trayIcon->showMessage("SignalNet", tr("Bell"), QSystemTrayIcon::Warning);
#endif
}

void MainWindow::readTemperature(QByteArray *data)
{
    ui->actionTemperature->setText(QString("%1 %2%3")
                                   .arg(tr("Temperature"))
                                   .arg(QSNRAWtoTemperature(data, 1), 0,'f', 1)
                                   .arg(QSNTypePostFix(9)));
#ifdef Q_OS_WIN
    if (!ui->actionTemperature->isVisible()) ui->actionTemperature->setVisible(true);
#endif
#ifdef Q_OS_LINUX
    if (!ui->actionTemperature->isEnabled()) ui->actionTemperature->setEnabled(true);
#endif
}

void MainWindow::setWindowsPosition()
{
    this->setWindowFlags(Qt::Drawer | Qt::WindowCloseButtonHint);
}

void MainWindow::setTrayIcon()
{
#ifdef Q_OS_WIN
    if (absencecount == -3) trayIcon->setIcon(QIcon(":/ico/iError.png"));
    if (absencecount == -2) trayIcon->setIcon(QIcon(":/ico/iPresence.png"));
    if (absencecount == 3) trayIcon->setIcon(QIcon(":/ico/iPresence.png"));
    else if (absencecount >= -1) trayIcon->setIcon(QIcon(":/ico/iAbsence.png"));
#endif
#ifdef Q_OS_LINUX
    if (absencecount == -3) trayIcon->setIcon(QIcon(":/ico/iErrorL.png"));
    if (absencecount == -2) trayIcon->setIcon(QIcon(":/ico/iPresenceL.png"));
    if (absencecount == 3) trayIcon->setIcon(QIcon(":/ico/iPresenceL.png"));
    else if (absencecount >= -1) trayIcon->setIcon(QIcon(":/ico/iAbsenceL.png"));
#endif
}

void MainWindow::checkPulse()
{
    oaclient->checkMac(ui->macEdit->text());

    if (lastPulse.msecsTo(QDateTime::currentDateTime()) > 200000) {
        if (absencecount > 0) mkey.sendKeyEventToSystem(Qt::Key_Control);
    }

    if (globalCursorPos == QCursor::pos()) return;
    if (absencecount >= 0 && absencecount < 3) {
        absencecount = -2;
        setTrayIcon();
    }
    globalCursorPos = QCursor::pos();
    lastPulse = QDateTime::currentDateTime();
}

void MainWindow::changeOAState(QString state)
{
    if (state == "presents") absencecount = 3;
    if (state == "absents" && absencecount > 0) absencecount --;
    if (absencecount == 0) {
        lock();
        absencecount = -1;
    }
    if (state == "Error connect") absencecount = -3;
    setTrayIcon();
}

void MainWindow::on_actionExit_triggered()
{
    disableClose = false;
    close();
}

void MainWindow::on_actionSettings_triggered()
{
    ui->stackedWidget->setCurrentIndex(0);
    resize(10,10);
    show();
}

void MainWindow::on_buttonCancel_clicked()
{
    close();
}

void MainWindow::on_buttonApply_clicked()
{
    oaclient->setPort(static_cast<quint16>(ui->oaPortEdit->value()));
    oaclient->setAddress(ui->oaAddressEdit->text());
    oaclient->setEnabled(ui->tcpSettings->isChecked());
    //    udpClient->setPort(static_cast<quint16>(ui->udpPortEdit->value()));
    //    udpClient->setAddress(ui->udpAddressEdit->text());
    //    udpClient->setKey(ui->udpKeyEdit->text());
    //    udpClient->setEnabled(ui->udpSettings->isChecked());
    writeSettings();
    close();
}

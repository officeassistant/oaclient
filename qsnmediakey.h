#ifndef QSNMEDIAKEY_H
#define QSNMEDIAKEY_H

#include <QObject>
#include <QVariant>
#include <QDebug>

class QsnMediaKey : public QObject
{
    Q_OBJECT
public:
    explicit QsnMediaKey(QObject *parent = nullptr);
    QVariant qkeyToOSkey(Qt::Key qtKey);
    void sendKeyEventToSystem(Qt::Key qtKey);
    void sendTwoKeyEventToSystem(Qt::Key qtKey1, Qt::Key qtKey2);

signals:

public slots:
};

#endif // QSNMEDIAKEY_H

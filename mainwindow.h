#ifndef MAINWINDOW_H
#define MAINWINDOW_H
// -- основные --
#define MemoryBlocks 85
#define DeviceTypeIndex 1001
#define IO_count 115

// -- cвойства --
#define ir_MediaPrevious 2
#define ir_MediaNext 6
#define ir_MediaTogglePlayPause 10
#define ir_MediaPlay 14
#define ir_MediaStop 18
#define ir_MediaPause 22
#define ir_VolumeUp 26
#define ir_VolumeDown 30
#define ir_VolumeMute 34
#define ir_Sleep 38
#define ir_Back 42
#define ir_Escape 46
#define ir_PageUp 50
#define ir_PageDown 54
#define ir_End 58
#define ir_Home 62
#define ir_Left 66
#define ir_Up 70
#define ir_Right 74
#define ir_Down 78
#define ir_Fullscreen 82
#define ir_Enter 86
#define ir_SwitchApp 90
#define ir_Poweroff 94
#define ir_Restart 98
#define ir_logout 102
#define ir_Hibernation 106
#define ir_Lock 110
#define ir_Close 114
#define ir_0 118
#define ir_1 122
#define ir_2 126
#define ir_3 130
#define ir_4 134
#define ir_5 138
#define ir_6 142
#define ir_7 146
#define ir_8 150
#define ir_9 154
#define ir_space 158

// -- выхода --
#define output_temperatureSetting 0
#define output_pulse 1

// -- входа --
#define input_alert 0
#define input_alarm 1
#define input_bell 2
#define input_ircode 3
#define input_poweroff 4
#define input_restart 5
#define input_logout 6
#define input_hibernation 7
#define input_lock 8
#define input_temperatureSetting 9
#define input_temperature 10

// -- отладка --



#include <QMainWindow>
#include <QSystemTrayIcon>
#include <QCloseEvent>
#include <QMenu>
#include <QTimer>
#include <QDateTime>
#include "qsnmediakey.h"
#include "qdesktopwidget.h"
#include "qsnshapes.h"
#include <QSettings>
#include <QProcess>
#include "qoatcpclient.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = Q_NULLPTR);
    ~MainWindow();

public slots:
    void closeEvent(QCloseEvent *event);
    void eventNumInput(int numInput, QByteArray Data);
    //----BM


private slots:
    void iconActivated(QSystemTrayIcon::ActivationReason reason);
    void on_actionExit_triggered();
    void on_actionSettings_triggered();
    void on_buttonCancel_clicked();
    void on_buttonApply_clicked();
    void checkPulse();
    void changeOAState(QString state);

private:
    Ui::MainWindow *ui;
    QSystemTrayIcon *trayIcon;
    QMenu *trayIconMenu;

    QOATCPclient *oaclient;

    QTimer checkTimer;
    QsnMediaKey mkey;
    bool disableClose;
    QPoint globalCursorPos;
    QDateTime lastPulse;

    int absencecount;

    void writeSettings();
    void readSettings();
 //   void decodeCommand(quint32 code);
    void poweroff();
    void logout();
    void hibernation();
    void restart();
    void lock();
    void fullScreen();
    void appClose();
    void switchApp();
    void showAlert(QByteArray *data);
    void showAlarm(QByteArray *data);
    void showBell();
    void readTemperature(QByteArray *data);
    void setWindowsPosition();
    void setTrayIcon();



};

#endif // MAINWINDOW_H
